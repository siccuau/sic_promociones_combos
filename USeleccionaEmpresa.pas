unit USeleccionaEmpresa;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.FMXUI.Wait,
  Data.DB, FireDAC.Comp.Client, FMX.StdCtrls, FMX.Controls.Presentation,
  FMX.Layouts, FMX.ListBox, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.Components,
  Data.Bind.DBScope,UPrincipal;

type
  TSeleccionaEmpresa = class(TForm)
    ListEmpresas: TListBox;
    Label1: TLabel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    FDConnection: TFDConnection;
    qryEmpresas: TFDQuery;
    qryEmpresasNOMBRE_CORTO: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    procedure FormShow(Sender: TObject);
    procedure ListEmpresasKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure btnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SeleccionaEmpresa: TSeleccionaEmpresa;

implementation

{$R *.fmx}

procedure TSeleccionaEmpresa.btnAceptarClick(Sender: TObject);
var
  FormPrincipal : TFormPrincipal;
begin
  FormPrincipal := TFormPrincipal.Create(nil);
  FormPrincipal.Conexion.Params.Values['Database'] := FDConnection.Params.Values['Database'].Replace('System\config.fdb','') + ListEmpresas.Selected.ItemData.Text + '.fdb';
  FormPrincipal.Conexion.Params.Values['Server'] := FDConnection.Params.Values['Server'];
  FormPrincipal.Conexion.Params.Values['User_Name'] := FDConnection.Params.Values['User_Name'];
  FormPrincipal.Conexion.Params.Values['Password'] := FDConnection.Params.Values['Password'];
  FormPrincipal.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormPrincipal.Visible := True;
  FormPrincipal.Show;

  Hide;
end;

procedure TSeleccionaEmpresa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
Application.Terminate;
end;

procedure TSeleccionaEmpresa.FormShow(Sender: TObject);
begin
qryEmpresas.Open();
end;

procedure TSeleccionaEmpresa.ListEmpresasKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  //if Key = #13 then btnAceptar.SetFocus;
end;

end.
