program PromocionesCombos;



uses
  FMX.Forms,
  UPrincipal in 'UPrincipal.pas' {FormPrincipal},
  ULogin in 'ULogin.pas' {Login},
  UConexiones in 'UConexiones.pas' {Conexiones},
  USeleccionaEmpresa in 'USeleccionaEmpresa.pas' {SeleccionaEmpresa},
  UEnvVars in 'UEnvVars.pas',
  ULicencia in 'ULicencia.pas',
  WbemScripting_TLB in 'WbemScripting_TLB.pas',
  UConfig in 'UConfig.pas' {Form1};

{$R *.res}

begin
  Application.Initialize;

  Application.CreateForm(TLogin, Login);
  Application.CreateForm(TFormPrincipal, FormPrincipal);
  Application.CreateForm(TConexiones, Conexiones);
  Application.CreateForm(TSeleccionaEmpresa, SeleccionaEmpresa);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
