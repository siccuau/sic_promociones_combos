unit UConfig;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.FMXUI.Wait,
  FMX.StdCtrls, FMX.Controls.Presentation, Data.DB, FireDAC.Comp.Client,
  FMX.ListBox, FMX.Edit, FMX.EditBox, FMX.SpinBox, FMX.Layouts,System.UIConsts,
  FMX.Colors;

type
  TForm1 = class(TForm)
    Conexion: TFDConnection;
    gb_tipos_promo: TGroupBox;
    chk_art_dscto_otros: TCheckBox;
    chk_dscto_art_combo: TCheckBox;
    chk_dscto_menor_precio: TCheckBox;
    lblRuta: TLabel;
    btnRuta: TButton;
    OpenDialog1: TOpenDialog;
    ImgFondo: TImageControl;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    lblPrueba: TLabel;
    ListBox1: TListBox;
    sbTama�o: TSpinBox;
    cmbColor: TComboBox;
    Label1: TLabel;
    Label3: TLabel;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    cmbNumPromo: TComboBox;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    Label5: TLabel;
    txtTiempo: TEdit;
    btnGuardar: TButton;
    ComboColorBox1: TComboColorBox;
    chk_promo_art: TCheckBox;
    chk_promo_linea: TCheckBox;
    chk_promo_grupo: TCheckBox;
    Label6: TLabel;
    ComboColorBox2: TComboColorBox;
    chk_dos_colores: TCheckBox;
    Label7: TLabel;
    procedure FormShow(Sender: TObject);
    procedure chk_art_dscto_otrosChange(Sender: TObject);
    procedure chk_dscto_art_comboChange(Sender: TObject);
    procedure chk_(Sender: TObject);
    procedure btnRutaClick(Sender: TObject);
    procedure sbTama�oChange(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cmbColorChange(Sender: TObject);
    procedure cmbNumPromoChange(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure ComboColorBox1Change(Sender: TObject);
    procedure ComboColorBox2Change(Sender: TObject);
    procedure chk_promo_artChange(Sender: TObject);
    procedure chk_promo_lineaChange(Sender: TObject);
    procedure chk_promo_grupoChange(Sender: TObject);
    procedure chk_dos_coloresChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}


procedure TForm1.btnGuardarClick(Sender: TObject);
begin
  if txtTiempo.Text<>'' then
  begin
   try
   Conexion.ExecSQL('update SIC_CONFIG_PROMO set TIEMPO_PROMO=:tiempo',[txtTiempo.Text]);
   Conexion.Commit;
   except
   ShowMessage('El tiempo indicado no es valido.');
   if Conexion.ExecSQLScalar('select TIEMPO_PROMO from SIC_CONFIG_PROMO')<>null then
   txtTiempo.Text:=Conexion.ExecSQLScalar('select TIEMPO_PROMO from SIC_CONFIG_PROMO')
   else txtTiempo.Text:='';
   Conexion.Rollback;
   end;
  end
  else ShowMessage('El tiempo no puede estar vacio.');
end;

procedure TForm1.btnRutaClick(Sender: TObject);
begin
  OpenDialog1.Filter:= 'Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png';
  if OpenDialog1.Execute then
  begin
    lblRuta.Text := OpenDialog1.FileName;
    try
    Conexion.ExecSQL('update SIC_CONFIG_PROMO set RUTA_FONDO=:fondo',[lblRuta.Text]);
    Conexion.Commit;
    ImgFondo.Bitmap.LoadFromFile(lblRuta.Text);
    except
    Conexion.Rollback;
    end;
  end
  else
  begin
    lblRuta.Text := '';
  end;
end;

procedure TForm1.chk_art_dscto_otrosChange(Sender: TObject);
begin
if chk_art_dscto_otros.IsChecked then Conexion.ExecSQL('update SIC_CONFIG_PROMO set ART_DSCTO_OTROS=''S''')
else Conexion.ExecSQL('update SIC_CONFIG_PROMO set ART_DSCTO_OTROS=''N''');
end;

procedure TForm1.chk_dos_coloresChange(Sender: TObject);
begin
if chk_dos_colores.IsChecked then
 begin
  Conexion.ExecSQL('update SIC_CONFIG_PROMO set DOS_COLORES=''S''');
  ComboColorBox2.Visible:=True;
  Label6.Visible:=True;
 end
else
 begin
  Conexion.ExecSQL('update SIC_CONFIG_PROMO set DOS_COLORES=''N''');
  ComboColorBox2.Visible:=False;
  Label6.Visible:=False;
 end;
end;

procedure TForm1.chk_dscto_art_comboChange(Sender: TObject);
begin
if chk_dscto_art_combo.IsChecked then Conexion.ExecSQL('update SIC_CONFIG_PROMO set DSCTO_ART_COMBO=''S''')
else Conexion.ExecSQL('update SIC_CONFIG_PROMO set DSCTO_ART_COMBO=''N''');
end;

procedure TForm1.chk_promo_artChange(Sender: TObject);
begin
if chk_promo_art.IsChecked then Conexion.ExecSQL('update SIC_CONFIG_PROMO set PROMO_ART=''S''')
else Conexion.ExecSQL('update SIC_CONFIG_PROMO set PROMO_ART=''N''');
end;

procedure TForm1.chk_promo_grupoChange(Sender: TObject);
begin
if chk_promo_grupo.IsChecked then Conexion.ExecSQL('update SIC_CONFIG_PROMO set PROMO_GRUPO=''S''')
else Conexion.ExecSQL('update SIC_CONFIG_PROMO set PROMO_GRUPO=''N''');
end;

procedure TForm1.chk_promo_lineaChange(Sender: TObject);
begin
if chk_promo_linea.IsChecked then Conexion.ExecSQL('update SIC_CONFIG_PROMO set PROMO_LINEA=''S''')
else Conexion.ExecSQL('update SIC_CONFIG_PROMO set PROMO_LINEA=''N''');
end;

procedure TForm1.chk_(Sender: TObject);
begin
if chk_dscto_menor_precio.IsChecked then Conexion.ExecSQL('update SIC_CONFIG_PROMO set DSCTO_MENOR_PRECIO=''S''')
else Conexion.ExecSQL('update SIC_CONFIG_PROMO set DSCTO_MENOR_PRECIO=''N''');
end;

procedure TForm1.cmbColorChange(Sender: TObject);
begin
 {lblPrueba.FontColor:=StringToAlphaColor(cmbColor.Items[cmbColor.ItemIndex]);
  try
  Conexion.ExecSQL('update SIC_CONFIG_PROMO set FUENTE_COLOR=:color',[cmbColor.Items[cmbColor.ItemIndex]]);
  Conexion.Commit;
  except
  Conexion.Rollback;
  end;  }
end;

procedure TForm1.cmbNumPromoChange(Sender: TObject);
begin
  try
  Conexion.ExecSQL('update SIC_CONFIG_PROMO set NUM_PROMO=:num',[cmbNumPromo.Items[cmbNumPromo.ItemIndex]]);
  Conexion.Commit;
  except
  Conexion.Rollback;
  end;
end;

procedure TForm1.ComboColorBox1Change(Sender: TObject);
var
colorStr:string;
begin
colorStr:=AlphaColorToString(ComboColorBox1.Color);
 lblPrueba.FontColor:=ComboColorBox1.Color;
  try
  Conexion.ExecSQL('update SIC_CONFIG_PROMO set FUENTE_COLOR=:color',[colorStr]);
  Conexion.Commit;
  except
  Conexion.Rollback;
  end;
end;

procedure TForm1.ComboColorBox2Change(Sender: TObject);
var
colorStr:string;
begin
colorStr:=AlphaColorToString(ComboColorBox2.Color);
 Label7.FontColor:=ComboColorBox2.Color;
  try
  Conexion.ExecSQL('update SIC_CONFIG_PROMO set FUENTE_COLOR2=:color',[colorStr]);
  Conexion.Commit;
  except
  Conexion.Rollback;
  end;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//Application.Terminate;
//Halt;
  try
  Conexion.ExecSQL('update SIC_CONFIG_PROMO set NUM_PROMO=:num',[cmbNumPromo.Items[cmbNumPromo.ItemIndex]]);
  Conexion.Commit;
  except
  Conexion.Rollback;
  end;
end;


procedure TForm1.FormCreate(Sender: TObject);

var fList: TStringList;
    i: Integer;
begin
    with ListBox1.Items do
    begin
      Add('Arial');
      Add('Bell MT');
      Add('Britannic');
      Add('Calibri');
      Add('Comic Sans MS');
      Add('Elephant');
      Add('French Script MT');
      Add('Gill Sans');
      Add('Impact');
      Add('Segoe');
      Add('Tahoma');
      Add('Verdana');
    end;

    with cmbColor.Items do
    begin
      Add('Aqua');
      Add('Black');
      Add('Blue');
      Add('Brown');
      Add('Darkgreen');
      Add('Darkred');
      Add('Darkblue');
      Add('Green');
      Add('Gray');
      Add('Orange');
      Add('Red');
      Add('White');
    end;

end;


procedure TForm1.FormShow(Sender: TObject);
begin
if Conexion.ExecSQLScalar('select ART_DSCTO_OTROS from SIC_CONFIG_PROMO')='S' then chk_art_dscto_otros.IsChecked:=true;
if Conexion.ExecSQLScalar('select DSCTO_ART_COMBO from SIC_CONFIG_PROMO')='S' then chk_dscto_art_combo.IsChecked:=true;
if Conexion.ExecSQLScalar('select DSCTO_MENOR_PRECIO from SIC_CONFIG_PROMO')='S' then chk_dscto_menor_precio.IsChecked:=true;
if Conexion.ExecSQLScalar('select PROMO_ART from SIC_CONFIG_PROMO')='S' then chk_promo_art.IsChecked:=true;
if Conexion.ExecSQLScalar('select PROMO_LINEA from SIC_CONFIG_PROMO')='S' then chk_promo_linea.IsChecked:=true;
if Conexion.ExecSQLScalar('select PROMO_GRUPO from SIC_CONFIG_PROMO')='S' then chk_promo_grupo.IsChecked:=true;

if Conexion.ExecSQLScalar('select DOS_COLORES from SIC_CONFIG_PROMO')='S' then
  begin
    chk_dos_colores.IsChecked:=True;
    ComboColorBox2.Visible:=True;
    Label6.Visible:=True;
  end
else
  begin
    chk_dos_colores.IsChecked:=False;
    ComboColorBox2.Visible:=False;
    Label6.Visible:=False;
  end;


if Conexion.ExecSQLScalar('select RUTA_FONDO from SIC_CONFIG_PROMO')<>null then
  begin
  lblRuta.Text:= Conexion.ExecSQLScalar('select RUTA_FONDO from SIC_CONFIG_PROMO');
  ImgFondo.Bitmap.LoadFromFile(lblRuta.Text);
  end;

if Conexion.ExecSQLScalar('select FUENTE_TAMANO from SIC_CONFIG_PROMO')<>null then
  begin
  sbTama�o.Text:=Conexion.ExecSQLScalar('select FUENTE_TAMANO from SIC_CONFIG_PROMO');
  sbTama�o.OnChange(nil);
  end;

if Conexion.ExecSQLScalar('select FUENTE_COLOR from SIC_CONFIG_PROMO')<>null then
  begin
  ComboColorBox1.Color:=StringToAlphaColor(Conexion.ExecSQLScalar('select FUENTE_COLOR from SIC_CONFIG_PROMO'));
  end;
if Conexion.ExecSQLScalar('select FUENTE_COLOR2 from SIC_CONFIG_PROMO')<>null then
  begin
  ComboColorBox2.Color:=StringToAlphaColor(Conexion.ExecSQLScalar('select FUENTE_COLOR2 from SIC_CONFIG_PROMO'));
  end;
if Conexion.ExecSQLScalar('select FUENTE_NOMBRE from SIC_CONFIG_PROMO')<>null then
  begin
  ListBox1.ItemIndex:=ListBox1.Items.IndexOf(Conexion.ExecSQLScalar('select FUENTE_NOMBRE from SIC_CONFIG_PROMO'));
  lblPrueba.Font.Family := ListBox1.Selected.Text;
  Label7.Font.Family := ListBox1.Selected.Text;
  end;

if Conexion.ExecSQLScalar('select NUM_PROMO from SIC_CONFIG_PROMO')<>null then
  begin
    if Conexion.ExecSQLScalar('select NUM_PROMO from SIC_CONFIG_PROMO')='4' then
    begin
    cmbNumPromo.ItemIndex:=0;
    end;
        if Conexion.ExecSQLScalar('select NUM_PROMO from SIC_CONFIG_PROMO')='6' then
    begin
    cmbNumPromo.ItemIndex:=1;
    end;
        if Conexion.ExecSQLScalar('select NUM_PROMO from SIC_CONFIG_PROMO')='8' then
    begin
    cmbNumPromo.ItemIndex:=2;
    end;
  end;

  if Conexion.ExecSQLScalar('select TIEMPO_PROMO from SIC_CONFIG_PROMO')<>null then
  begin
  txtTiempo.Text:=Conexion.ExecSQLScalar('select TIEMPO_PROMO from SIC_CONFIG_PROMO');
  end;

end;

procedure TForm1.ListBox1Click(Sender: TObject);
begin
lblPrueba.Font.Family := ListBox1.Selected.Text;
Label7.Font.Family := ListBox1.Selected.Text;
  try
  Conexion.ExecSQL('update SIC_CONFIG_PROMO set FUENTE_NOMBRE=:fuente',[ListBox1.Selected.Text]);
  Conexion.Commit;
  except
  Conexion.Rollback;
  end;
end;

procedure TForm1.sbTama�oChange(Sender: TObject);
begin
lblPrueba.Font.Size:=sbTama�o.Value;
Label7.Font.Size:=sbTama�o.Value;
  try
  Conexion.ExecSQL('update SIC_CONFIG_PROMO set FUENTE_TAMANO=:tamano',[sbTama�o.Value]);
  Conexion.Commit;
  except
  Conexion.Rollback;
  end;
end;

end.
